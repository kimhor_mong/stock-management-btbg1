package database;

import com.kshrd.student.Product;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class Row {
    public static int setRowNumber(int rowNum) throws SQLException {
        Connection con = DBConnection.getConnect();
        PreparedStatement ps = con.prepareStatement("UPDATE row_num SET row_num = ?");
        ps.setInt(1, rowNum);

        int updatedRow = ps.executeUpdate();

        ps.close();
        con.close();

        return updatedRow;
    };

    public static int getRowNumber() throws SQLException {
        Connection con = DBConnection.getConnect();
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT row_num FROM row_num LIMIT 1;");

        rs.next();
        int rowNumber = rs.getInt("row_num");

        rs.close();
        stmt.close();
        con.close();

        return rowNumber;
    };
}
