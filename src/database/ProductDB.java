package database;

import com.kshrd.student.Product;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ProductDB {
    public static ArrayList<Product> all() throws SQLException {
        Connection con = DBConnection.getConnect();
        Statement stat = con.createStatement();
        ResultSet rs = stat.executeQuery("SELECT * FROM PRODUCT");

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        ArrayList<Product> products = new ArrayList<>();
        while (rs.next()){
            Product product = new Product();
            product.setId(rs.getInt("id"));
            product.setName(rs.getString("name"));
            product.setUnitPrice(rs.getFloat("unit_price"));
            product.setStockQty(rs.getInt("stock_qty"));
            product.setImportedDate(dateFormat.format(rs.getDate("imported_date")));
            products.add(product);
        }
        rs.close();
        stat.close();
        con.close();

        return products;
    };

    public static boolean backUp(List<Product> products) throws SQLException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh_mm_ss_dd_MM_yyyy");
        String date = dateFormat.format(new Date());
        String tableName = "product_" + date;

        Connection con = DBConnection.getConnect();
        Statement stat = con.createStatement();

        String query = String.format("CREATE TABLE %s " +
                "(id serial primary key, " +
                "name varchar, " +
                "unit_price float, " +
                "stock_qty int, " +
                "imported_date date);", tableName);

        stat.executeUpdate(query);

        PreparedStatement ps = con.prepareStatement("INSERT INTO "+ tableName +" (name, " +
                "unit_price, stock_qty, imported_date) values(?, ?, ?, TO_DATE(?, 'dd-MM-yyyy'));");

        for (Product product : products) {
            ps.setString(1, product.getName());
            ps.setFloat(2, product.getUnitPrice());
            ps.setInt(3, product.getStockQty());
            ps.setString(4, product.getImportedDate());
            ps.executeUpdate();
        }


        ps.close();
        stat.close();
        con.close();
        return true;
    }

    public static boolean save(List<Product> products) throws SQLException {
        Connection con = DBConnection.getConnect();
        Statement stat = con.createStatement();
        stat.executeUpdate("TRUNCATE TABLE product RESTART IDENTITY;");

        PreparedStatement ps = con.prepareStatement("INSERT INTO PRODUCT(name, " +
                "unit_price, stock_qty, imported_date) values(?, ?, ?, TO_DATE(?, 'dd-MM-yyyy'));");

        for (Product product : products) {
            ps.setString(1, product.getName());
            ps.setFloat(2, product.getUnitPrice());
            ps.setInt(3, product.getStockQty());
            ps.setString(4, product.getImportedDate());
            ps.executeUpdate();
        }
        stat.close();
        ps.close();
        con.close();
        return true;
    }


    public static boolean restore(String tableName) throws SQLException {
        Connection con = DBConnection.getConnect();
        Statement stat = con.createStatement();

        stat.executeUpdate("TRUNCATE TABLE product RESTART IDENTITY;");

        stat.executeUpdate("INSERT INTO product (name, " +
                "unit_price, stock_qty, imported_date) " +
                "SELECT name, unit_price, stock_qty, imported_date FROM " + tableName);

        stat.close();
        con.close();
        return true;
    }

}
