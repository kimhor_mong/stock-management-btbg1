package com.kshrd.student;
import database.ProductDB;
import database.Row;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

public class ProductView {
    private List<Product> products;
    private static Console scanner = new Console();

    private int row;
    private int page;
    private int allPage;
    private int total;

    private boolean wasChanged = false;

    public ProductView() { }

    public void start() throws InterruptedException, IOException, SQLException {
        showText();
        String inputOption;
        do {
            mainMenu();
            System.out.print("Command --> ");
            inputOption = scanner.readAndReturnString("").toUpperCase();
            System.out.println();
            switch (inputOption) {
                case "*":
                    displayProduct();
                    break;
                case "W" :
                    addProduct();
                    break;
                case "R" :
                    readProduct();
                    break;
                case "U":
                    updateProduct();
                    break;
                case "D":
                    deleteProduct();
                    break;
                case "F":
                    displayFirstPage();
                    break;
                case "P":
                    displayPrevious();
                    break;
                case "N":
                    displayNextPage();
                    break;
                case "L":
                    displayLastPage();
                    break;
                case "S":
                    displaySearchByName();
                    break;
                case "G":
                    displaySpecificPage();
                    break;
                case "SE":
                    setRow();
                    break;
                case "SA":
                    saveToDatabase();
                    break;
                case "BA":
                    backUpToDatabase();
                    break;
                case "RE":
                    //restore
                    break;
                case "H":
                    helpMenu();
                    break;
                case "E":
                    exitProgram();
                    break;
                default: showError();
            }
        } while (!inputOption.equals("E"));
    }

    private void showText() throws InterruptedException, SQLException {
        CreateThread createThread = new CreateThread();
        createThread.start();
        createThread.join();

        StopWatch.start();
        products = ProductDB.all();
        System.out.println("Current time loading: " + StopWatch.stop());

        page = 1;
        row = Row.getRowNumber();
        total = products.size();
        allPage = getPage();
    }

    private int getPage(){
       int allPage = total / row;
        if(total % row > 0){
            allPage ++;
        }
        return allPage;
    }

    private List<Product> getCurrentPage(){
        int start = (page - 1) * row;
        int limit = row;
        int remain = total % row;
       if(page == allPage){
           start = remain == 0 ? total - row : total - remain;
       }
        if( remain > 0 && page == allPage){
            limit = remain;
        }
        return products.stream().skip(start).limit(limit).collect(Collectors.toList());
    }
    private List<Product> getFirstPage(){
        page = 1;
       return products.stream().limit(row).collect(Collectors.toList());
    }
    private List<Product> getLastPage(){
        page = allPage;
        int start = (total % row)==0 ? total - row : total - (total % row);
        return products.stream().skip(start).collect(Collectors.toList());
    }
    private List<Product> getNextPage(){
        page++;
        int start = 0;
        int limit = row;
        int remain = total % row;

        if(page == allPage ){
            start = remain==0 ? total - row : total - remain;
        } else if(page > allPage){
            page = 1;
            start = 0;
        } else {
            start = (page - 1) * row;
        }

        if( remain > 0 && page == allPage){
            limit = remain;
        }
        return products.stream().skip(start).limit(limit).collect(Collectors.toList());
    }
    private List<Product> getPreviousPage(){
        page--;
        if(page == 0){
            page = allPage;
        }
        if( page == allPage){
            return getLastPage();
        }
        int start = (page -1) *row;
        return products.stream().skip(start).limit(row).collect(Collectors.toList());
    }
    private  void setRow() throws IOException, SQLException {
        int row = scanner.readAndReturnInteger("Set row: ","[1-9]\\d*");
        this.row = row;
        allPage = getPage();
        Row.setRowNumber(row);
    }

    private List<Product> gotoPage(int page){
        this.page = page;
       return getCurrentPage();
    }
    private List<Product> searchProductByName(String productName){
        return products.stream()
                .filter(product -> product.getName().toLowerCase().contains(productName.toLowerCase()))
                .collect(Collectors.toList());
    }
    private void saveToDatabase() throws  SQLException {
        String option = scanner.readAndReturnString("Do you want changes to database? [Y/y] or [N/n]: ", "[Y|y|N|n]");
        if (option.equalsIgnoreCase("y")){
           ProductDB.save(products);
            Message.formatSuccessMessage("   Saved to file successfully   ");
        }
    }

    private void backUpToDatabase() throws SQLException {
        ProductDB.backUp(products);
    }

    private void addProduct(){
        Product product = new Product();

        System.out.println("Product ID : " + product.getId());
        String name = scanner.readAndReturnString("Product's Name: ");
        float unitPrice = scanner.readAndReturnFloat("Product's Price: ");
        int qty = scanner.readAndReturnInteger("Product's Qty: ","[1-9]\\d*");

        product.setName(name);
        product.setUnitPrice(unitPrice);
        product.setStockQty(qty);

        showSpecificProduct(product);

        String option = scanner.readAndReturnString("Are you sure to add this record? [Y/y] or [N/n]: ", "[Y|y|N|n]");

        if(option.equalsIgnoreCase("y")){
            products.add(product);
            total++;
            allPage = getPage();
            Message.formatSuccessMessage("   Product was added successfully    ");
            wasChanged = true;

        }
    }

    private void readProduct(){
        int id = scanner.readAndReturnInteger("Read by ID: ","[1-9]\\d*");
        Product product = searchProductById(id);
        if(product == null){
            Message.formatErrorMessage("   Not found   ");
        } else {
                showSpecificProduct(product);
        }
    }
    private void updateProduct(){
        int id = scanner.readAndReturnInteger("Enter product's ID: ","[1-9]\\d*");
        Product product = searchProductById(id);
        Product newProduct = null;
        if(product == null){
            Message.formatSuccessMessage("   Not found   ");
        } else {
            showSpecificProduct(product);
            updateMenu();
            newProduct = new Product(product.getId(), product.getName(),
                    product.getUnitPrice(), product.getStockQty(), product.getImportedDate());

            int option = scanner.readAndReturnInteger("Option: ","[1-5]");

            String name;
            float unitPrice;
            int qty;

            switch (option){
                case 1:
                    name = scanner.readAndReturnString("Product's name: ");
                    unitPrice = scanner.readAndReturnFloat("Product's Price: ");
                    qty = scanner.readAndReturnInteger("Stock Quantity: ","[1-9]\\d*");
                    newProduct.setName(name);
                    newProduct.setUnitPrice(unitPrice);
                    newProduct.setStockQty(qty);
                    break;
                case 2:
                    name = scanner.readAndReturnString("Product's name: ");
                    newProduct.setName(name);
                    break;
                case 3:
                    qty = scanner.readAndReturnInteger("Stock Quantity: ","[1-9]\\d*");
                    newProduct.setStockQty(qty);
                    break;
                case 4:
                    unitPrice = scanner.readAndReturnFloat("Product's Price: ");
                    newProduct.setUnitPrice(unitPrice);
                    break;
                case 5:
                    System.out.println("back to menu");
                    break;
            }
            if(option !=5){
                showSpecificProduct(newProduct);
                String choice = scanner.readAndReturnString("Are you sure to add this record? [Y/y] or [N/n]: ", "[Y|y|N|n]");
                if(choice.equalsIgnoreCase("y"))
                {
                    product.setName(newProduct.getName());
                    product.setUnitPrice(newProduct.getUnitPrice());
                    product.setStockQty(newProduct.getStockQty());
                    Message.formatSuccessMessage("   Product was updated   ");

                    wasChanged = true;
                }
            }
        }
    }

    private void deleteProduct(){
        int id = scanner.readAndReturnInteger("Enter product's ID: ","[1-9]\\d*");
        Product product = searchProductById(id);

        if(product == null){
            Message.formatErrorMessage("   Not found   ");
        } else {
            showSpecificProduct(product);
            String choice = scanner.readAndReturnString("Are you sure to delete this record? [Y/y] or [N/n]: ", "[Y|y|N|n]");
            if(choice.equalsIgnoreCase("y"))
            {
                Message.formatSuccessMessage(String.format("   Product with ID %d was deleted   ",product.getId()));
                products.remove(product);
                total--;
                allPage = getPage();

                wasChanged = true;
            }
        }
    }

    private void displayProduct(){
        showProductDetail(getCurrentPage());
    }
    private Product searchProductById(int id){
        return products.stream()
                .filter(product -> product.getId() == id).findFirst().orElse(null);
    }
    private void showProductDetail(List<Product> products){
        Table table = getTableWithHeader();
        CellStyle columnCellStyle = new CellStyle(CellStyle.HorizontalAlign.left);
        for (Product product: products) {
            table.addCell(String.valueOf(product.getId()), columnCellStyle);
            table.addCell(product.getName(), columnCellStyle);
            table.addCell(String.valueOf(product.getUnitPrice()), columnCellStyle);
            table.addCell(String.valueOf(product.getStockQty()), columnCellStyle);
            table.addCell(product.getImportedDate(), columnCellStyle);
        }
        System.out.println(table.render());
        pagination();
    }
    private  void showSpecificProduct(Product product){
        Table table = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE, ShownBorders.SURROUND);
        CellStyle columnCellStyle = new CellStyle(CellStyle.HorizontalAlign.left);

        table.addCell(String.format("ID            : %d        ",product.getId()), columnCellStyle);
        table.addCell(String.format("Name          : %s        ",product.getName()), columnCellStyle);
        table.addCell(String.format("Unit Price    : %.2f      ",product.getUnitPrice()), columnCellStyle);
        table.addCell(String.format("Quantity      : %d        ",product.getStockQty()), columnCellStyle);
        table.addCell(String.format("Imported Date : %s        ",product.getImportedDate()), columnCellStyle);
        System.out.println();
        System.out.println(table.render());
        System.out.println();
    }
    private void pagination(){

        Message.formatForPagination(String.format("   Page : %d of %d                  Total Record :  %d",page, allPage, total));
    }
    private Table getTableWithHeader(){
        Table table = new Table(5, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE, ShownBorders.ALL);
        CellStyle headerCellStyle = new CellStyle(CellStyle.HorizontalAlign.center);
        table.addCell("ID", headerCellStyle);
        table.addCell("NAME", headerCellStyle);
        table.addCell("Unit Price", headerCellStyle);
        table.addCell("Quantity", headerCellStyle);
        table.addCell("Imported Date", headerCellStyle);
        return table;
    }
    private void displayFirstPage(){
        showProductDetail(getFirstPage());
    }
    private void displayLastPage(){
        showProductDetail(getLastPage());
    }
    private void displayPrevious(){
        showProductDetail(getPreviousPage());
    }
    private void displayNextPage(){
        showProductDetail(getNextPage());
    }
    private void displaySpecificPage(){
        int page= scanner.readAndReturnInteger("Go to Page: ", "[1-9]\\d*");
        if (page>allPage){
            page=allPage;
        }
        showProductDetail(gotoPage(page));
    }
    private void displaySearchByName(){
        String name = scanner.readAndReturnString("Enter product's name: ");
          List<Product> products = searchProductByName(name);
          if(products.size()==0){
              Message.formatErrorMessage("   Not found   ");
          }else {
              showProductDetail(products);
          }
    }
    private void mainMenu(){
        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.center);
        Table t = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.SURROUND);
        t.addCell("    *)Display   | W)rite    | R)ead     | U)pdate    | D)elete   | F)irst   |P)revious  |N)ext  |L)last      ", numberStyle);
        t.addCell("    S)earch     | G)oto     | Se)t row  | Sa)ve      | Ba)ck up  | Re)store |H)elp      |E)xit               ", numberStyle);
        System.out.println(t.render());
    }
    private void updateMenu(){
        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.center);
        Table t = new Table(1, BorderStyle.UNICODE_BOX, ShownBorders.SURROUND);
        t.addCell("    1. All      2.Name      3.Quantity      4.Unit Price        5. Back to Menu     ", numberStyle);
        System.out.println("\nWhat do you want to update?");
        System.out.println(t.render());
    }
    private void helpMenu(){
        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.center);
        Table t = new Table(1, BorderStyle.UNICODE_HEAVY_BOX, ShownBorders.SURROUND);
        t.addCell(" 1.     Press   *  : Display all recode of Product                     ", numberStyle);
        t.addCell(" 2.     Press   w  : Add new Product                                   ", numberStyle);
        t.addCell("        Press   w#promote-unitprice-qty : shortcut of add new product  ", numberStyle);
        t.addCell(" 3.     Press   r  : read content any content                          ", numberStyle);
        t.addCell("        Press   r#proId : shortcut for read product by Id              ", numberStyle);
        t.addCell(" 4.     Press   u  : Update data                                       ", numberStyle);
        t.addCell(" 5.     Press   d  : Delete data                                       ", numberStyle);
        t.addCell("        Press   d#proId : shortcut for delete product by Id            ", numberStyle);
        t.addCell(" 6.     Press   f  : Display first page                                ", numberStyle);
        t.addCell(" 7.     Press   p  : Display previous page                             ", numberStyle);
        t.addCell(" 8.     Press   n  : Display next page                                 ", numberStyle);
        t.addCell(" 9.     Press   l  : Display past page                                 ", numberStyle);
        t.addCell(" 10.    Press   s  : Search product by name                            ", numberStyle);
        t.addCell(" 11.    Press   sa : Save record to file                               ", numberStyle);
        t.addCell(" 12.    Press   ba : Backup data                                       ", numberStyle);
        t.addCell(" 13.    Press   re : Restore data                                      ", numberStyle);
        t.addCell(" 14.    Press   h  : Help                                              ", numberStyle);
        System.out.println(t.render());
    }

    private void exitProgram() throws IOException, SQLException {
        if(wasChanged){
            saveToDatabase();
        }
     System.out.println("(^-^) Good Bye! (^-^)");
    }
    private void showError(){
        Message.formatErrorMessage("   INVALID INPUT   ");
    }
}
