package com.kshrd.student;

import java.time.Clock;

public class StopWatch {
    private static long begin =0l;
    private static long end =0l;

    static void start(){
        begin = Clock.systemDefaultZone().millis();
    }

    static long stop(){
        end = Clock.systemDefaultZone().millis();
        return end-begin;
    }
}
