package com.kshrd.student;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;
public class Message {
    public static void formatErrorMessage(String massage){
        Table table = new Table(1, BorderStyle.CLASSIC_COMPATIBLE_LIGHT_WIDE, ShownBorders.SURROUND);
        CellStyle columnCellStyle = new CellStyle(CellStyle.HorizontalAlign.left);
        table.addCell(massage, columnCellStyle);
        System.out.println();
        System.out.println(table.render());
        System.out.println();
    }
    public static void formatSuccessMessage(String massage){
        Table table = new Table(1, BorderStyle.DESIGN_CAFE_WIDE, ShownBorders.SURROUND);
        CellStyle columnCellStyle = new CellStyle(CellStyle.HorizontalAlign.left);
        table.addCell(massage, columnCellStyle);
        System.out.println();
        System.out.println(table.render());
        System.out.println();
    }
    public static void formatForPagination(String massage){
        Table table = new Table(1, BorderStyle.DESIGN_CURTAIN, ShownBorders.SURROUND);
        CellStyle columnCellStyle = new CellStyle(CellStyle.HorizontalAlign.left);
        table.addCell(massage, columnCellStyle);
        System.out.println(table.render());
    }

}
