package com.kshrd.student;

import java.util.Scanner;
import java.util.regex.Pattern;

public class Console {
    private static Scanner scanner;

    public Console(){
        scanner = new Scanner(System.in);
    }

    public void promptEnterKey(){
        System.out.println("\nPress \"ENTER\" to continue...");
        scanner.nextLine();
    }

    public int readAndReturnInteger(String message, String regEx){
        String input;
        do{
            System.out.print(message);
            input = scanner.nextLine();
        } while (!isValidInteger(input,regEx));
        return Integer.parseInt(input);
    }

    public float readAndReturnFloat(String message){
        String input;
        do{
            System.out.print(message);
            input = scanner.nextLine();
        } while (!isValidFloat(input));
        return Float.parseFloat(input);
    }

    public String readAndReturnString(String message){
        String input;
        do {
            System.out.print(message);
            input = scanner.nextLine();
        } while (!isEmptyString(input));
        return input;
    }

    public String readAndReturnString(String message, String regEx){
        String input;
        do {
            System.out.print(message);
            input = scanner.nextLine();
        } while (!isValidOption(input, regEx));
        return input;
    }


    private static boolean isValidInteger(String input, String regEx){
        boolean isMatched = Pattern.matches(regEx, input);
        if(!isMatched){
            Message.formatErrorMessage("   INPUT IS INVALID   ");

            return false;
        }
        return true;
    }

    private static boolean isValidFloat(String input){
        // regEx for float number
        String regEx = "^(?:[1-9]\\d*)?(?:\\.\\d+)?$";
        boolean isMatched = Pattern.matches(regEx, input);
        if(!isMatched){
            Message.formatErrorMessage("   INPUT IS INVALID   ");
            return false;
        }
        return true;
    }

    private static boolean isEmptyString(String input){
        boolean isEmpty = !Pattern.matches("^(?=\\s*\\S).*$", input);
        if(isEmpty){
            Message.formatForPagination("   INPUT IS EMPTY   ");
            return false;
        }
        return true;
    }

    private static boolean isValidOption(String input, String regEx){
        boolean isMatched = Pattern.matches(regEx, input);
        if(!isMatched){
            Message.formatErrorMessage("   INPUT IS INVALID   ");
            return false;
        }
        return true;
    }
}

