package com.kshrd.student;
import java.io.IOException;
import java.sql.SQLException;

public class Main {
    public static void main(String[] args) throws IOException, InterruptedException, SQLException {
      ProductView productView = new ProductView();
         productView.start();
    }
}
