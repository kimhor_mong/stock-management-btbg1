package com.kshrd.student;

import java.text.SimpleDateFormat;
import java.util.Date;


public class Product {
    private static int autoIncrement;

    private int id;
    private String name;
    private float unitPrice;
    private int stockQty;
    private String importedDate;

    public Product(){
        id = ++autoIncrement;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        importedDate = dateFormat.format(new Date());
    }

    public Product(int id, String name, float unitPrice, int stockQty, String importedDate) {
        this.id = id;
        this.name = name;
        this.unitPrice = unitPrice;
        this.stockQty = stockQty;
        this.importedDate = importedDate;
    }

    public static void setAutoIncrement(int autoIncrement) {
        Product.autoIncrement = autoIncrement;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(float unitPrice) {
        this.unitPrice = unitPrice;
    }

    public int getStockQty() {
        return stockQty;
    }

    public void setStockQty(int stockQty) {
        this.stockQty = stockQty;
    }

    public String getImportedDate() {
        return importedDate;
    }

    public void setImportedDate(String importedDate) {
        this.importedDate = importedDate;
    }

    @Override
    public String toString() {
        return  id + " " + name + " " + unitPrice + " " + stockQty + " " + importedDate + "\n";
    }
}
